kopano-webapp (3.5.14+dfsg1.orig-1) unstable; urgency=medium

  * [4c29027] New upstream version 3.5.14+dfsg1.orig

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sun, 08 Dec 2019 15:23:53 +0100

kopano-webapp (3.5.13+dfsg1-1) unstable; urgency=medium

  * [cd7be6e] New upstream version 3.5.13+dfsg1
  * [e367039] rebuild patch queue from patch-queue branch

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sun, 24 Nov 2019 11:14:47 +0100

kopano-webapp (3.5.12+dfsg1-1) unstable; urgency=medium

  * [27aee26] New upstream version 3.5.12+dfsg1
  * [a1545dc] rebuild patch queue from patch-queue branch
  * [0276382] autopkgtest: adjust testing

 -- Carsten Schoenert <c.schoenert@t-online.de>  Fri, 01 Nov 2019 18:13:36 +0100

kopano-webapp (3.5.10+dfsg1-1) unstable; urgency=medium

  * [9c055ec] New upstream version 3.5.10+dfsg1
  * [7117c6d] debian/control: increase Standards-Version to 4.4.1
  * [fe29d44] dh: move over to debhelper-compat
  * [a2c591f] rebuild patch queue from patch-queue branch

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sat, 05 Oct 2019 14:55:06 +0200

kopano-webapp (3.5.9+dfsg1-1) unstable; urgency=medium

  * [e3d5e48] New upstream version 3.5.9+dfsg1
  * [4654083] rebuild patch queue from patch-queue branch
  * [31eed61] source lintian: ignore more warning about long lines
     Also ignore waenings about oidc-client.js and tokenizr.js.

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sun, 11 Aug 2019 19:22:35 +0200

kopano-webapp (3.5.8+dfsg1-2) unstable; urgency=medium

  * [40939f0] remove php-gettext as package dependency

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sun, 28 Jul 2019 09:56:52 -0300

kopano-webapp (3.5.8+dfsg1-1) unstable; urgency=medium

  * [744e940] New upstream version 3.5.8+dfsg1
  * [18fbd1c] d/copyright: update copyright information
     Adding a new file which we didn't have tracked yet.
  * [871fc90] rebuild patch queue from patch-queue branch
  * [2c6da71] debian/control: increase Standards-Version to 4.4.0
     No further changes needed.

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sat, 27 Jul 2019 13:33:42 -0300

kopano-webapp (3.5.6+dfsg1-1) unstable; urgency=medium

  * [627b6b3] New upstream version 3.5.6+dfsg1
  * [79f588b] rebuild patch queue from patch-queue branch
  * [b4a3931] d/copyright: update copyright information
     Since upstream version 3.5.4 some new additional software is included
     which is required to be reflected in the copyright file.
  * [c983579] d/rules: exclude the installation of *.md files

 -- Carsten Schoenert <c.schoenert@t-online.de>  Thu, 30 May 2019 11:22:51 +0200

kopano-webapp (3.5.3+dfsg1-1) unstable; urgency=medium

  * [a292ded] New upstream version 3.5.3+dfsg1
  * [5a04a67] rebuild patch queue from patch-queue branch

 -- Carsten Schoenert <c.schoenert@t-online.de>  Thu, 07 Mar 2019 19:51:26 +0100

kopano-webapp (3.5.2+dfsg1-1) unstable; urgency=medium

  * [63c1d6f] New upstream version 3.5.2+dfsg1
  * [57242ac] rebuild patch queue from patch-queue branch

 -- Carsten Schoenert <c.schoenert@t-online.de>  Tue, 05 Feb 2019 19:28:34 +0100

kopano-webapp (3.5.1-1) unstable; urgency=medium

  [ Carsten Schoenert ]
  * [c4517b5] New upstream version 3.5.1
  * [5416e27] source lintian: ignore warning about long line
     Ignore warning about KQLParser.js which has one long line.

  [ Martijn Alberts ]
  * [c0aae23] autopkgtest: use another method to find CSS IDs
     Use now another check method while searching for an CSS ID. Due this the
     check searches for another ID too.

 -- Carsten Schoenert <c.schoenert@t-online.de>  Fri, 18 Jan 2019 19:01:32 +0100

kopano-webapp (3.5.1~rc1+dfsg1-1) unstable; urgency=medium

  * [1df56c9] debian/control: increase Standards-Version to 4.3.0
     No further changes needed.
  * [2b72826] d/control: remove npm package from B-D (again)
     The npm package was a Build-Depends after kopano-webapp versions 3.4.22,
     but no version of these releases was ever uploaded. npm isn't needed now
     any more.
  * [3756879] autopkgtest: create needed store before testing
     Since kopanocore version > 8.6.0 the user needs to get a store, updating
     the autopkgtest to fulfil this requirement.
  * [32228fd] autopkgtest: update test to work in headless mode
     The phantomjs driver in selenium is abandoned for some time, it's
     suggested to work with the headless mode of the chromedriver or with
     firefox.
     Rework of the test to work with chromium in headless mode.
     Thanks Guido for helping out with the lambda functions!
  * [66c1e9e] New upstream version 3.5.1~rc1+dfsg1
  * [878097f] rebuild patch queue from patch-queue branch
     removed patch (fixed upstream):
     server-do-not-define-JSONException-as-function.patch
  * [748e428] autopkgtest: search correct CSS class names
     Obviously the CSS class names have changed a bit more, we need to search
     for other names now to login and send an email.
  * [c56d0f3] autopkgtest: move over to Python3 based packages
  * [6b4563d] d/control: increase depending version on php-mapi
     The autopkgtest has detected that we need a recent version of php-mapi
     (which is provided by the kopanocore package) with a version >= 8.6.92.
  * [e1a92d4] k-w-common.install: also install the tokenizr JS file
     The file tokenizr.min.js isn't installed by the Makefile from upstream,
     working around this and pick the file directly from the source folder.

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sun, 13 Jan 2019 12:52:59 +0100

kopano-webapp (3.5.0+dfsg1-1) unstable; urgency=medium

  * [c7188ec] d/rules: detect PHP_VERSION dynamically
     We need to know the version of PHP in some postinst scripts. Read the PHP
     version from the installed php$VERSION-cli package which is getting
     installed as a Build-Dependency from php-dev.
  * [cf82604] d/control: add B-D on php-dev
  * [0ed0592] d/rules: some sanity checking addition
     Ensure we could read the PHP version from the system, otherwise a further
     build is useless.
  * [6320c46] New upstream version 3.5.0+dfsg1
  * [de94789] rebuild patch queue from patch-queue branch
     Removed all patches that have modified the existing po files, all
     modifications are applied upstream now. The non existing language es_CA
     still needs to be removed by the patch queue.
    added patch:
     server-do-not-define-JSONException-as-function.patch
  * [0ff6616] Build_Depends: add new needed packages
     The new upstream version is relying on npm and phpmd as build
     dependencies.
  * [858ac95] d/control: remove ant* packages from B-D
     But we don't need any ant related packages as build dependencies any more.
  * [a24d168] d/rules: drop ant related call
     As the build system isn't using ant any more we need to adjust the build
     in debian/rules. Upstream is now working with a classical Makefile.
  * [0ffdb83] d/rules: remove potential execution rights on php files
     We don't need execution rights on the php files shipped in the -common
     package.
  * [c451b2b] k-w-common: increase depending version on php-mapi
     kopano-webapp is heavily relying on php-mapi from src:kopanocore. Recent
     kopanocore packages are build against php7.3 so we need to increase the
     depending version of php-mapi.
  * [b31e457] k-w-nginx: update postinst script
     The postinst for the nginx package was missing a reload of php-fpm at the
     end of the postinst script. Reworking some informal output in the script.
  * [2060987] k-w-nginx: update postrm script
     No functional changes, but also updating the informal output of the
     script.
  * [11bf5b1] k-w-apache2: update postinst script
     The apache2 postinst script needs to enable the mapi module while
     configuring the package in case of installation or update.
  * [ff3cbf3] k-w-lighttpd: always remove site config while purging
     (Closes: #909599)
  * [980c627] k-w-lighttpd: use dynamical PHPVERSION in postinst
     Don't use a hardcoded PHP version within the postinst script, the version
     will substituted with the version from the build so we don't need to
     modify it every time a new PHP version is coming.
  * [d83bfa3] k-w-lighttpd: update email contact in README
     Since the ML is now moved to the domain alioth-lists.debian.net a small
     update is needed in the README.
  * [d2552c3] k-w-lighttpd: update README about http usage
     And one more update to the README file for kopano-webapp-lighttpd, add
     some information about the wanted non working http only access to
     kopano-webapp.
  * [7900c38] d/t/control: autopkgtest needs root rights
     A small update to the autotest, but more changes are needed to get the
     autopkgtest working again.

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sun, 23 Dec 2018 18:04:49 +0100

kopano-webapp (3.4.22+dfsg1-1) unstable; urgency=medium

  * [d65e82d] New upstream version 3.4.22+dfsg1
  * [f1056a0] rebuild patch queue from patch-queue branch
  * [c2bf107] d/kopano-webapp-common: install and use Kopano fonts
     Installing the Kopano specific fonts into the system folder and linking
     them into the package again.

 -- Carsten Schoenert <c.schoenert@t-online.de>  Fri, 21 Sep 2018 19:43:39 +0200

kopano-webapp (3.4.20+dfsg1-1) unstable; urgency=medium

  * [38175e2] New upstream version 3.4.20+dfsg1
  * [3b98a14] rebuild patch queue from patch-queue branch
    added patches:
    ar.UTF-8-add-minor-comment-and-Project-Id-Version.patch
    ar_AE.UTF-8-add-minor-comment-and-Project-Id-Version.patch
    ar_BH.UTF-8-add-minor-comment-and-Project-Id-Version.patch
    ar_EG.UTF-8-add-minor-comment-and-Project-Id-Version.patch
    ar_IQ.UTF-8-add-minor-comment-and-Project-Id-Version.patch
    ar_JO.UTF-8-add-minor-comment-and-Project-Id-Version.patch
    ar_KW.UTF-8-add-minor-comment-and-Project-Id-Version.patch
    ar_LB.UTF-8-add-minor-comment-and-Project-Id-Version.patch
    ar_LY.UTF-8-add-minor-comment-and-Project-Id-Version.patch
    ar_MA.UTF-8-add-minor-comment-and-Project-Id-Version.patch
    ar_OM.UTF-8-add-minor-comment-and-Project-Id-Version.patch
    ar_QA.UTF-8-add-minor-comment-and-Project-Id-Version.patch
    ar_SA.UTF-8-add-minor-comment-and-Project-Id-Version.patch
    ar_SD.UTF-8-add-minor-comment-and-Project-Id-Version.patch
    ar_SY.UTF-8-add-minor-comment-and-Project-Id-Version.patch
    ar_TN.UTF-8-add-minor-comment-and-Project-Id-Version.patch
    ar_YE.UTF-8-add-minor-comment-and-Project-Id-Version.patch
  * [9c77a7e] debian/control: increase Standards-Version to 4.2.1
     No further changes needed.
  * [59f4a02] bump compat and debhelper to version 11
  * [5cf3a8f] debian/copyright: update copyright information

 -- Carsten Schoenert <c.schoenert@t-online.de>  Mon, 27 Aug 2018 20:18:01 +0200

kopano-webapp (3.4.19+dfsg1-1) unstable; urgency=medium

  * [06cd21e] New upstream version 3.4.19+dfsg1
  * [4ffc195] rebuild patch queue from patch-queue branch

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sat, 28 Jul 2018 14:12:22 +0800

kopano-webapp (3.4.17+dfsg1-1) unstable; urgency=medium

  * [d3dbe35] New upstream version 3.4.17+dfsg1
  * [fb602d7] rebuild patch queue from patch-queue branch
  * [423df90] update Maintainers to new contact address
     Updating the Maintainer email address to 
     pkg-giraffe-maintainers@alioth-lists.debian.net
  * [05e8fb1] debian/control: increase Standards-Version to 4.1.5
     No further changes needed.

 -- Carsten Schoenert <c.schoenert@t-online.de>  Fri, 13 Jul 2018 20:54:19 +0200

kopano-webapp (3.4.16-1) unstable; urgency=medium

  * [5019d3e] uscan: also match minor versions with more than one digest
  * [58e8132] New upstream version 3.4.16
  * [e6355a5] rebuild patch queue from patch-queue branch

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sat, 23 Jun 2018 10:59:22 +0200

kopano-webapp (3.4.15+dfsg1-1) unstable; urgency=medium

  * [5b4a74b] New upstream version 3.4.15+dfsg1
  * [294aef1] rebuild patch queue from patch-queue branch

 -- Carsten Schoenert <c.schoenert@t-online.de>  Thu, 31 May 2018 07:21:43 +0200

kopano-webapp (3.4.13+dfsg1-1) unstable; urgency=medium

  [ Carsten Schoenert ]
  * [680949a] use PHP_VERSION from one central place
     We need the used PHP version in various files and places, define the PHP
     version within debian/rules and substitute this into the needed files. By
     this we won't forget to collate the PHP version in the other files.
  * [51d7810] kopano-webapp-nginx: also substitute PHP version here
     Use also the PHP version from debian/rules for kopano-webapp-nginx.
  * [0372b15] k-w-nginx: enable web site activation
     By this version we will enable the Nginx web site configuration for
     kopano-webapp.
  * [fb30e45] k-w-nginx: create dedicated user config file
     Providing a dedicated file where the user needs to add the needed web
     site configuration and modifications.
  * [8a9c68c] k-w-n: modify README after changed behavior
  * [260aaa1] New upstream version 3.4.13+dfsg1
  * [66c03a4] rebuild patch queue from patch-queue branch
     Added a patch for the new shipped l10n files for Arabic (Algeria).
  * [9feeca2] debian/control: increase Standards-Version to 4.1.4
     No further changes needed.
  * [33af810] debian/rules: override the dh_autotest target
     kopano-webapp comes now with automated tests which doesn't work inside
     the buildd infrastructure and we need to disable.

 -- Carsten Schoenert <c.schoenert@t-online.de>  Thu, 17 May 2018 12:13:52 +0200

kopano-webapp (3.4.10+dfsg1-1) unstable; urgency=medium

  [ Carsten Schoenert ]
  * [813691e] New upstream version 3.4.10+dfsg1
  * [2e1203f] rebuild patch queue from patch-queue branch

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sat, 07 Apr 2018 18:11:45 +0200

kopano-webapp (3.4.9+dfsg1-1) unstable; urgency=medium

  [ Carsten Schoenert ]
  * [e0eb87a] New upstream version 3.4.9+dfsg1
  * [4c4e258] rebuild patch queue from patch-queue branch
  * [2bdcb0b] d/kopano-webapp-apache2.conf: adding upstream modifications

 -- Carsten Schoenert <c.schoenert@t-online.de>  Fri, 16 Mar 2018 10:26:20 +0100

kopano-webapp (3.4.7+dfsg1-1) unstable; urgency=medium

  [ Carsten Schoenert ]
  * [e30d290] New upstream version 3.4.7+dfsg1
  * [99c8675] rebuild patch queue from patch-queue branch
  * [ef340eb] d/control: adding Rules-Requires-Root to source section

 -- Carsten Schoenert <c.schoenert@t-online.de>  Mon, 05 Mar 2018 19:30:16 +0100

kopano-webapp (3.4.6+dfsg1-1) unstable; urgency=medium

  [ Carsten Schoenert ]
  * [4a7b3cc] New upstream version 3.4.6+dfsg1
  * [9471ff5] rebuild patch queue from patch-queue branch
  * [b85f8d1] debian/control: adding new field Rules-Requires-Root

 -- Carsten Schoenert <c.schoenert@t-online.de>  Fri, 16 Feb 2018 19:22:29 +0100

kopano-webapp (3.4.5+dfsg1-1) unstable; urgency=medium

  [ Chris Lamb ]
  * [abd06ef] ignore *.js.map file while installation (Closes: #887989)

  [ Carsten Schoenert ]
  * [9a9c678] ignore *debug.js file too while installation
  * [5c1cc85] New upstream version 3.4.5+dfsg1
  * [9ea9ec1] rebuild patch queue from patch-queue branch
  * [4aba63e] adjust Vcs fields to salsa.debian.org

 -- Carsten Schoenert <c.schoenert@t-online.de>  Fri, 02 Feb 2018 20:52:16 +0100

kopano-webapp (3.4.4+dfsg1-1) unstable; urgency=medium

  [ Carsten Schoenert ]
  * [f804b49] New upstream version 3.4.4+dfsg1
  * [cd3b3dd] rebuild patch queue from patch-queue branch
  * [5977cd7] kopano-webapp: switch to upstream apparmor profile
     Thanks to Kopano which have accepted a pull request to include the
     apparmor profile Guido has prepared and we included since version
     3.4.0+dfsg1-1.

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sun, 21 Jan 2018 19:34:06 +0100

kopano-webapp (3.4.3+dfsg1-1) experimental; urgency=medium

  [ Carsten Schoenert ]
  * [95af975] New upstream version 3.4.1+dfsg1
  * [877e01f, 73d82f3] debian/control: increase Standards-Version to 4.1.2
     and 4.1.3
     No further changes are needed.
  * [e445a2c] debian/gbp.conf: don't relay hard on xz compression
  * [c22dd62] New upstream version 3.4.2+dfsg1
  * [69f69f8] k-w-apache2: enhance postinst procedure
     Don't let the website configuration disabled and try to activate the
     configuration while installing k-w for the first time. Don't force to
     re-enable k-w in case the user has disabled the current configuration
     and the installation of k-w is a update.
     Closes: #876668
  * [c4740a8] k-w-apache2: adding a postrm script
     Remove the symlink from /e/a/conf-enabled in case the user is purging the
     package.
  * [0753d23] k-w-a: modify README after changed behavior
     Adding some notes about the current behavior for k-w-a to the README of
     the package, explain how the website configuration is intended and why it
     is now enabled after the installation of k-w-a.
  * [2277f4b] New upstream version 3.4.3+dfsg1
  * [7036d6c] lintian-overrides: add one more override
     Adding a l-o to a almost readable and not minified JS file that is only
     used for unit testing.

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sat, 06 Jan 2018 20:43:35 +0100

kopano-webapp (3.4.0+dfsg1-1) experimental; urgency=medium

  [ Guido Günther ]
  * [edc719f] postinst: Fix typo
  * [2c298e3] apparmor: add hat name if apache module is enabled
  * [7310d19] Add apparmor profile
  * [e9f346e] Create empty plugin folder
  * [f03dd82, 273adee, 3c6251c] uscan: simplify the rule set
     Use a more strictly kopano-webapp specific set up, which means
     * Don't prefer rc and beta over releases
     * Handle dot in front of beta and rc versions numbers

  [ Jelle van der Waa ]
  * [5b7d1ec] rules: remove left-over files plugin references
     Remove cruft in debian/rules which isn't needed and used any more.

  [ Carsten Schoenert ]
  * [429e8ed] gbp.conf: don't remove powerpaste plugin
     Don't remove the Powerpaste plugin in future imports, it's free
     software.
  * [7613a3c] New upstream version 3.3.1+dfsg1
  * [05cc6e2] debian/watch: update content due needed dversionmangle
     We mark the source tarbal with the 'dfsg' suffix to show we have filtered
     out some non DFSG compatible parts. Thus needs to be mangled by uscan.
  * [8ac43f8] debian/control: increase Standards-Version to 4.1.1
     No further changes are needed.
  * [d20d14b] debian/control: remove not needed ${misc:Recommends}
     Just a clean out of these not needed variables in Architecture: all
     packages.
  * [c1d3521] s/lintian-overrides: add extra JS files
    We need to add some minor overrides related to the powerpaste plugin.
  * [98edf68] debian/rules: remove License.txt from powerpaste folder
  * [3216194] debian/copyright: add information for powerpaste plugin
  * [3738132] debian/watch: update due changed GitHub organisation
  * [c586f03] autopkgtest: adjust chromiumdriver -> chromium-driver
  * [49801f2] k-w-c.lintian-overrides: avoid lintian warning about empty folder
  * [f313807] New upstream version 3.4.0+dfsg1
  * [845c8f8] rebuild patch queue from patch-queue branch
     Adding few i18n related patches for QS purpose of the language files. To
     be forwarded upstream.
  * [443d2ce] README.source: update content
  * [78c842f] src:kopano-webapp: add some additional fields
     Adding now we have a working Git repository on Alioth also the VCS fields
     and also a Homepage field pointing to the upstream GitHub tree.
  * [568e86b] debian/watch: (re)adding dversionmangle due 'dfsg' suffix
  * [421c4fe] lintian: adding override for kopanowebappdings.ttf

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sun, 19 Nov 2017 13:24:04 +0100

kopano-webapp (3.3.1-1) experimental; urgency=medium

  * Initial Upload
    Closes: #783640

  [ Carsten Schoenert ]
  * [2322e52] Imported Upstream version 2.1.0~rc1
  * [50df361] rebuild patch queue from patch-queue branch
  * [b2eff8b] d/c-u-t.sh: remove more files and folders from upstream
  * [3585010] debian/copyright: adopt new included 'powerpaste' plugin
  * [c74eb6d] debian/rules: removing unneeded license text files
  * [434ff7b] debian/copyright: update after removing PHP-2.02 related file
  * [2299c8b] debian/control: zarafa-webapp-files needs php5-curl
  * [7292ac2] debian/rules: remove unneeded sabredav folder before install
  * [d1fe95d] remove unneeded libjs-jac build dependency
  * [489cc91] Imported Upstream version 2.1.0
  * [1ff73a9] debian/rules: don't be verbose for now any longer
  * [d1bcb83] debian/control: adding Jelle as uploader
  * [0c3e08a] d/zarafa-webapp.conf: adding a Apache vhost config
  * [dcbe334] adopting z-w.{postinst,prerm} scripts to ssl configuration
  * [d908aad] remove no longer needed zarafa-webapp.preinst
  * [fd664d9] lintian: do not depend on apache2 only
  * [f546c82] adding debhelper for Apache2 stuff
  * [0626557] adding build dependency on msgfmt for translations
  * [e65445b] apache2: adding a redirect from http to https for z-w
  * [c6a2216] adding recommends on zarafa-server
  * [2ac54ab] add base test skeleton for autopkg tests
  * [ca4fc4a] debian/watch: adopt to new upstream layout

  [ Guido Günther ]
  * [5ea7457] Add simple logon smoke test

  [ Carsten Schoenert ]
  * [a0d201f] adopting helper script to new upstream source naming
  * [067fc05] Imported Upstream version 2.1.1
  * [a27f159] rebuild patch queue from patch-queue branch
  * [0f59851] zarafa-webapp: adding new introduced file robots.txt

  [ Guido Günther ]
  * [720609d] Build-Depend on gettext

  [ Carsten Schoenert ]
  * [b8357a0] Imported Upstream version 2.1.2
  * [4629e29] debian/control: remove packages due no longer shipped source
  * [91869b2] remove control files for zarafa-webapp-dropboxattachment
  * [8d86206] remove control files for zarafa-webapp-feedback
  * [a8783d0] remove control files for zarafa-webapp-salesforce
  * [c832a00] remove control files for zarafa-webapp-shellgame
  * [0e7c23e] remove control files for zarafa-webapp-spreed
  * [27236b6] remove control files for zarafa-webapp-statslogging
  * [729f457] remove control files for zarafa-webapp-sugarcrm
  * [a76ea97] remove control files for zarafa-webapp-zperformance
  * [f110a27] zarafa-webapp: moving .htaccess to /e/z/w
  * [0c95837] debian/copyright: adopt file changes from upstream
  * [aafea49] Imported Upstream version 2.2.0
  * [b3b31f1] rebuild patch queue from patch-queue branch
  * [a8d4935] remove control files for zarafa-webapp-extbox
  * [88ca1ea] remove control files for zarafa-webapp-oauthlib
  * [9fc2ed7] remove control files for zarafa-webapp-pdfbox
  * [2c87d91] remove control files for zarafa-webapp-webodf
  * [6a11adf] adjust zarafa-webapp due upstream changes
  * [6c28a47] increase Standard-Versions to 3.9.8
  * [0d28488] debian/gbp.conf: adding some filters for not wanted stuff
  * [ad4317e] debian/control: change source package to kopano-webapp
  * [bd563f2] New upstream version 3.1.0
  * [2085ece] *-contactfax: rename to kopano-webapp-contactfax
  * [48f4e79] *-folderwidgets: rename to kopano-webapp-folderwidgets
  * [a92fa47] *-gmaps: rename to kopano-webapp-gmaps
  * [3636b55] *-pimfolders: rename to kopano-webapp-pimfolders
  * [eba27c8] *-quickitems: rename to kopano-webapp-quickitems
  * [8704b99] *-titlecounter: rename to kopano-webapp-titlecounter
  * [7afe573] *-webappmanual: rename to kopano-webapp-webappmanual
  * [b81126a] *-zdeveloper: rename to kopano-webapp-zdeveloper
  * [cc0615d] remove no longer living AddOns
  * [4407d64] *-webapp: rename to kopano-webapp
  * [cfc47ba] debian/rules: reflecting name change to kopano*
  * [21d8b39] lintian-overrides: adding some source related overrides
  * [3c1d213] rebuild patch queue from patch-queue branch
  * [0e98834] kopano-webapp.lintian-overrides: avoid tinymce related messages
  * [4ac8145] debian/rules: don't try to remove tinymce-plugings
  * [9ecab75] debian/copyright: update the copyright information
  * [7bbece3] kopano-wepapp: adopting .htaccess directives from upstream
  * [61f07fd] New upstream version 3.1.1
  * [c1baaab] debian/README.source: update package to kopano-webapp
  * [fb993ed] debian/control: switch PHP related packages to v7
  * [b6a1d5e] New upstream version 3.2.0
  * [82084f7] move kopano-webapp.* into kopano-webapp-common.*
  * [3fa6f11] splitting of the apache2 configuration into own package
  * [ccd8b38] adding separate package for nginx configuration
  * [4f24b48] Adding basic stuff for a lighttpd configuration
  * [85072ac] debian/watch: moving over the watch mechanism to kopano.io
  * [fc1543c] debian/control: let kopano-webapp-common depend on php-mapi
  * [d0d803c] rename k-w-a.R.debian -> k-w-a.R.Debian so dh can pick it up
  * [9a44e4e] kopano-webapp-nginx.conf: normalize indentation
  * [7f5e5f1] kopano-webapp-apache2: small additions to the dh files
  * [9f5f68d] kopano-webapp-lighttpd: enable additional package
  * [2f11092] New upstream version 3.3.0
  * [2c3cd94] debian/copyright: update after upstream changes
  * [8a7a0dc] lintian-overrides: updates due upstream changes
  * [30311c0] rebuild patch queue from patch-queue branch
  * [a85e807] autopkg: rename all references of zarafa into kopano
  * [f232c93] New upstream version 3.3.1
  * [2c214d0] debian/watch: use the GitHub mirror from Kopano
  * [350dd3c] debian/control: increase Standards-Version to 4.1.0
  * [8dbde67] debian/control: Update email contact for Jelle v.d. Waa
  * [3b62a82] bump debhelper and compat version to 10
  * [966ae5f] d/rules: use DEB_* variables for entries from changelog
  * [145e9ef] debian/copyright: adjust URI http -> https
  * [2ed5c31] d/kopano-webapp-common.links: symlink tinymce license.txt

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sun, 23 Jul 2017 19:48:43 +0200

zarafa-webapp (2.1.0~beta1-1) experimental; urgency=medium

  [ Carsten Schoenert ]
  * [9f82148] switch to format 3.0 (quilt)
  * [fad8453] debian/control: adding some needed Builds-Depends
  * [913774f] debian/control: increase Standards-Version
  * [5a4d600] debian/control: change Section for source package
  * [47cbb07] debian/control: remove duplicated Section field
  * [5d2f214] debian/control: additional Build-Depends for JS things
  * [56e5797] debian/rules: adding some linking to external packages
  * [447aff1] debian/rules: fixing some lintian issues
  * [7a6c96d] debian/control: adding a package dependency on libjs-jac
  * [2cd294b] debian/copyright: starting rework of the copyright infos
  * [21ad476] debian/rules: add missed installation for titelcounter
  * [a170558] remove package *-facebook[widget] and *-twidget

  [ Jelle van der Waa ]
  * [6aebeca] Add Object.js file
  * [84881d4] Add missing license for various files
  * [8aba5db] pdfjs license is from Mozilla
  * [52de9e5] Add salesforce external dependency copyright notice

  [ Carsten Schoenert ]
  * [346497b] debian/copyright: more rework
  * [e1fe0d7] debian/watch: adding a watch file
  * [13f09fa] doing linking to libjs-jac the right way
  * [5a1c805] d/c-u-t.sh: adding helper script to create cleaned upstream source
  * [01c8fc1] lintian: adding override for symlinking

  [ Jelle van der Waa ]
  * [ea0d7d3] Resolve lintian package-contains-broken-symlink
  * [2c39ef3] debian/control: extend descriptions of WebApp plugins.

  [ Carsten Schoenert ]
  * [41fc720] debian/rules: remove some empty folders from packages
  * [daa15a1] Imported Upstream version 2.1.0~beta1
  * [ad06b8f] jquery isn't shipped anymore, no build-dep needed
  * [73201b5] libjs-extjs isn't a build-dep anymore
  * [33293b2] libjs-prettify isn't a build-dep anymore
  * [c25b69e] create-upstream-tarball.sh: update due upstream changes
  * [3c8d159] adding needed changes to package z-w-pdfbox
  * [9b96e7e] debian/rules: remove the license file from tinymce
  * [794be70] debian/rules: move package names into ${PACKAGES}
  * [3a28e19] debian/changelog: fix month acronym in old release
  * [27f353a] debhelper: increase debhelper version to 9
  * [61a4e0c] zarafa-webapp: add some more dependencies
  * [f453dfd] lintian: fix wrong phrased LGPL description
  * [9b12af3] debian/rules: use proper dpkg-parsechangelog command
  * [6c0a011] rebuild patch queue from patch-queue branch
  * [fa2652d] debian/rules: copy deploy/ to debian/tmp/ for dh usage
  * [e2e7f7e] z-w-browsercompatibility: switch to dh template usage
  * [a6b972e] z-w-clockwidget: switch to dh template usage
  * [d5d7e1e] z-w-contactfax: switch to dh template usage
  * [19569e6] z-w-dropboxattachment: switch to dh template usage
  * [b1627c5] z-w-extbox: switch to dh template usage
  * [e0a72a1] z-w-feedback: switch to dh template usage
  * [5516be7] z-w-files: switch to dh template usage
  * [7ce319e] z-w-folderwidgets: switch to dh template usage
  * [96c044d] z-w-gmaps: switch to dh template usage
  * [c12d041] z-w-oauthlib: switch to dh template usage
  * [79f47bd] z-w-pdfbox: switch to dh template usage
  * [596adbd] z-w-pimfolder: switch to dh template usage
  * [60eacd7] z-w-quickitems: switch to dh template usage
  * [63a7416] z-w-salesforce: switch to dh template usage
  * [c02e64d] z-w-shellgame: switch to dh template usage
  * [b1ade55] z-w-spreed: switch to dh template usage
  * [c8f79cf] z-w-statslogging: switch to dh template usage
  * [bfd82cb] z-w-sugarcrm: switch to dh template usage
  * [04e0969] z-w-titlecounter: switch to dh template usage
  * [f55f831] z-w-webappmanual: switch to dh template usage
  * [4fe747f] z-w-webodf: switch to dh template usage
  * [0e1dfe3] z-w-xmpp: switch to dh template usage
  * [e6f512e] z-w-zdeveloper: switch to dh template usage
  * [3056837] z-w-zperformance: switch to dh template usage
  * [9fbac53] zarafa-webapp: switch to dh template usage
  * [28f3294] debian/zarafa-webapp.postrm: adding directory purging

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sun, 13 Sep 2015 20:07:21 +0200

zarafa-webapp (2.0.2-1) unstable; urgency=low

  * Current release

 -- Zarafa Development Team <development@zarafa.com>  Thu, 04 Sep 2014 11:27:09 +0200
