#!/bin/sh

# postinst script for kopano-webapp-nginx
# see: dh_installdeb(1)

# summary of how this script can be called:
#                           --- $1 ---          --- $2 ---     --- $3 ---               --- $4 ---
#            <new-postinst> 'configure'         <most-recently-configured-version>
#            <old-postinst> 'abort-upgrade'     <new version>
#   <conflictor's-postinst> 'abort-remove'      'in-favour'     <package>                <new-version>
#                <postinst> 'abort-remove'
# <deconfigured's-postinst> 'abort-deconfigure' 'in-favour'     <failed-install-package> <version>     'removing' <conflicting-package> <version>
#
# for details, see http://www.debian.org/doc/debian-policy/ or the
# debian-policy package

set -e
#set -x # uncomment for verbose output

PHPVERSION="@version@"
NGINX_SITES_AVAILABLE="/etc/nginx/sites-available"
NGINX_SITES_ENABLED="/etc/nginx/sites-enabled"

case "$1" in
    configure)
        if [ -e /usr/lib/php/php-maintscript-helper ]; then
            . /usr/lib/php/php-maintscript-helper

            # calling php_invoke to enable 'mapi' for the PHP module fpm
            php_invoke enmod ${PHPVERSION} fpm mapi
        fi
        # preparations made, try to activate or reload nginx config
        if test -n "$2"; then
            # we are updating to a new version
            if [ `find "${NGINX_SITES_ENABLED}/" -type f -iname kopano-webapp*.conf | wc -l` -gt 0 ]; then
                # the user has enabled site(s) for kopano-webapp
                if [ -x /etc/init.d/nginx ]; then
                    if [ -s /run/nginx.pid ] && pidof /usr/sbin/nginx >/dev/null; then
                        echo "[kopano-webapp-nginx] Triggering reload of Nginx config ..."
                        invoke-rc.d nginx reload || true
                    fi
                 else
                    echo "[kopano-webapp-nginx] Couldn't find Nginx and reload the configuration. Package nginx not installed?"
                fi
            fi
        else
            # we are on a first time installation
            if [ ! -L "${NGINX_SITES_ENABLED}/kopano-webapp.conf" ]; then
                echo "[kopano-webapp-nginx] Enable kopano-webapp-nginx site config ..." >&2
                ln -s ${NGINX_SITES_AVAILABLE}/kopano-webapp.conf ${NGINX_SITES_ENABLED}/kopano-webapp.conf
				# check if Nginx is installed ...
                if [ -x /etc/init.d/nginx ]; then
                    # ... and is running
                    if [ -s /run/nginx.pid ] && pidof /usr/sbin/nginx >/dev/null; then
                        echo "[kopano-webapp-nginx] Triggering reload of Nginx config ..."
                        invoke-rc.d nginx reload || true
                    fi
                 else
                    # we should never came to this as we depend on the nginx package
                    echo "Couldn't find Nginx and activate the kopano-webapp configuration. Package nginx not installed?"
                fi
            fi
        fi
        invoke-rc.d php${PHPVERSION}-fpm reload || true
    ;;

    abort-upgrade|abort-remove|abort-deconfigure)
    ;;

    *)
        echo "postinst called with unknown argument \`$1'" >&2
        exit 1
    ;;
esac

#DEBHELPER#

exit 0
